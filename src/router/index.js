import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Rules from '@/components/Rules'
import About from '@/components/About'
import Contacts from '@/components/Contacts'
import Companies from '@/components/Companies'
import CompanyAdd from '@/components/CompanyAdd'
import CompanyAbout from '@/components/CompanyAbout'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            component: Home,
            name: 'home',
            path: '/'
        },
        {
            component: Companies,
            name: 'companies',
            path: '/companies'
        },
        {
            component: CompanyAdd,
            name: 'company-add',
            path: '/company-add'
        },
        {
            component: CompanyAbout,
            name: 'company-about',
            path: '/company/:id'
        },
        {
            component: Rules,
            name: 'rules',
            path: '/rules'
        },
        {
            component: About,
            name: 'about',
            path: '/about'
        },
        {
            component: Contacts,
            name: 'contacts',
            path: '/contacts'
        }
    ]
})