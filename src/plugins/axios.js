import axios from 'axios'

export const HTTP = axios.create({
    baseURL: 'https://api.waps.su/api/'
    //baseURL: 'http://spa.rus/api/'
})